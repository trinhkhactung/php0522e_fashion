<?php
include_once('./config/Connect.php');

class Product extends Connect
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return array
     */
    public function all() {
        $sql = "SELECT products.id, products.name, products.images, products.quantity, products.price, category.title FROM products, category WHERE category.id = products.category_id ORDER BY products.created_at DESC";
        $pre = $this->pdo->prepare($sql);
        $pre->execute();
        return $pre->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $categoryId
     * @param $name
     * @param $price
     * @param $images
     * @param $quantity
     * @param $description
     * @param $active
     * @return bool
     */
    public function create($categoryId, $name, $price, $images, $quantity, $description, $active) {
        $sql = "INSERT INTO `products`(`category_id`, `name`, `price`, `images`, `quantity`, `description`, `active`) VALUES (:category_id, :name, :price, :images, :quantity, :description, :active)";
        $pre = $this->pdo->prepare($sql);
        $pre->bindParam(':category_id', $categoryId);
        $pre->bindParam(':name', $name);
        $pre->bindParam(':price', $price);
        $pre->bindParam(':images', $images);
        $pre->bindParam(':quantity', $quantity);
        $pre->bindParam(':description', $description);
        $pre->bindParam(':active', $active);
        return $pre->execute();
    }

    /**
     * @return array
     */
    public function getAllCategory() {
        $sql = "SELECT id, title FROM category";
        $pre = $this->pdo->prepare($sql);
        $pre->execute();
        return $pre->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete($id) {
        $sql = "DELETE FROM products WHERE id = :id";
        $pre = $this->pdo->prepare($sql);
        $pre->bindParam(':id', $id);
        return $pre->execute();
    }

    public function find($id) {
        $sql = "SELECT *FROM products WHERE id = :id";
        $pre = $this->pdo->prepare($sql);
        $pre->bindParam(':id', $id);
        $pre->execute();
        return $pre->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param $categoryId
     * @param $name
     * @param $price
     * @param $images
     * @param $quantity
     * @param $description
     * @param $active
     * @return bool
     */
    public function update($id, $categoryId, $name, $price, $images, $quantity, $description, $active) {
        $sql = "UPDATE `products` SET `category_id` = :category_id, `name` = :name, `price` = :price, `images` = :images, `quantity` = :quantity, `description` = :description, `active` = :active WHERE id = :id";
        $pre = $this->pdo->prepare($sql);
        $pre->bindParam(':id', $id);
        $pre->bindParam(':category_id', $categoryId);
        $pre->bindParam(':name', $name);
        $pre->bindParam(':price', $price);
        $pre->bindParam(':images', $images);
        $pre->bindParam(':quantity', $quantity);
        $pre->bindParam(':description', $description);
        $pre->bindParam(':active', $active);

        return $pre->execute();
    }
}