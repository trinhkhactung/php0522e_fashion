<?php
include_once('../config/Connect.php');

class Server extends Connect
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDescriptionProduct($id) {
        $sql = "SELECT description FROM products WHERE id = :id";
        $pre = $this->pdo->prepare($sql);
        $pre->bindParam(':id', $id);
        $pre->execute();
        $product = $pre->fetch(PDO::FETCH_ASSOC);
        return $product['description'];
    }
}