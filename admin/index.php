<?php
    session_start();
    ob_start();
    if (!isset($_SESSION['id_account'])) {
        header('Location: ../auth');
    }

//    include_once('./config/Connect.php');
?>

<!DOCTYPE html>
<html lang="en">

<?php include_once('./layout/head.php'); ?>

<body>
    <div id="wrapper">
        <?php include_once('./layout/nav.php'); ?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <?php
                    $page = 'dashboard';

                    if (isset($_GET['page'])) {
                        $page = $_GET['page'];
                    }

                    switch ($page) {
                        case 'dashboard' :
                            include_once('./views/dashboard.php');
                            break;

                        case 'product' :
                            include_once './controller/ProductController.php';
                            $product = new ProductController;
                            break;

                        default: {
                            break;
                        }
                    }
                ?>
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>

    <?php include_once('./layout/script.php'); ?>
</body>

</html>
