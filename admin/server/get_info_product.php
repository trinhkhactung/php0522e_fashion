<?php
    include_once '../Controller/ServerController.php';

    if (isset($_GET['id'])) {
        $id = $_GET['id'];
        $product = new ServerController;
        return $product->index($id);
    }

    return false;