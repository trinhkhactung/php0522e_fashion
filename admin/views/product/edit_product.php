<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Edit product
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="index.php?page=product">List product</a>
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-8 col-12">
        <form name="frm_add_product" id="frm_add_product" method="POST" action="index.php?page=product&method=update&id=<?= $product['id']; ?>" enctype="multipart/form-data">

            <div class="form-group">
                <label>Name <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="name" id="name" required minlength="3" maxlength="200" placeholder="Name product" value="<?= !empty($product['name']) ? $product['name'] : '' ?>">
            </div>

            <div class="form-group">
                <label>Category <span class="text-danger">*</span></label>
                <select class="form-control" name="category" id="category">
                    <?php foreach ($categories as $categorie) { ?>
                        <option <?= ($product['category_id'] == $categorie['id']) ? 'selected' : '' ?> value="<?= $categorie['id']; ?>">
                            <?= $categorie['title']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label>Price <span class="text-danger">*</span></label>
                <input type="number" step="0.01" class="form-control" name="price" id="price" required min="0" value="<?= !empty($product['price']) ? $product['price'] : '' ?>">
            </div>

            <div class="form-group">
                <label>Avatar</label>
                <input type="file" name="image" id="image">
                <img src="../img/product/<?= $product['images']; ?>" width="80" alt="" style="margin-top: .8rem;">
            </div>

            <div class="form-group">
                <label>Quanity <span class="text-danger">*</span></label>
                <input type="number" class="form-control" name="quantity" id="quantity" required min="0" value="<?= !empty($product['quantity']) ? $product['quantity'] : '' ?>">
            </div>

            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control ckeditor" name="description" id="ckeditor" rows="3"><?= !empty($product['description']) ? $product['description'] : '' ?></textarea>
            </div>

            <div class="form-group">
                <label>Active</label>
                <label class="radio-inline">
                    <input <?= ($product['active']) ? 'checked' : '' ?> type="radio" name="active" id="active_show" value="1" checked> Show
                </label>

                <label class="radio-inline">
                    <input <?= !empty($product['active'] == 0) ? 'checked' : '' ?> type="radio" name="active" id="active_hide" value="0"> Hide
                </label>
            </div>

            <button type="submit" class="btn btn-primary" name="submit_update_product">Save</button>
            <button type="reset" class="btn btn-danger">Reset</button>

        </form>
    </div>
</div>
<!-- /.row -->