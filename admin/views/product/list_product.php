<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Product <small> Overview</small>
        </h1>

        <?php include_once './views/notification/notification.php'; ?>

        <ol class="breadcrumb">
            <li class="active">
                <a href="index.php?page=product&method=add"> Add product</a>
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Quanity</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                <?php
                    foreach ($products as $product) {
                ?>
                    <tr>
                        <td>
                            <img src="../img/product/<?= $product['images']; ?>" width="80" alt="">
                        </td>
                        <td><?= $product['name']; ?></td>
                        <td><?= $product['title']; ?></td>
                        <td><?= "$".$product['price']; ?></td>
                        <td><?= $product['quantity']; ?></td>
                        <td class="text-right">
                            <i style="cursor: pointer;" class="fa fa-fw fa-eye text-danger more_detail" data-id="<?= $product['id']; ?>" data-toggle="modal" data-target="#detail_product"></i>
                            <a href="index.php?page=product&method=edit&id=<?= $product['id']; ?>"><i class="fa fa-fw fa-edit"></i></a>
                            <a href="index.php?page=product&method=delete&id=<?= $product['id']; ?>" onclick="return confirm('Are you sure delete?');"><i class="fa fa-fw fa-trash text-danger"></i></a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>

            <!-- Modal -->
            <div class="modal fade" id="detail_product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Detail product</h4>
                        </div>
                        <div class="modal-body">
                            Mo ta san pham
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>