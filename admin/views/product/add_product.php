<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Add product
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="index.php?page=product">List product</a>
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->

<div class="row">
    <div class="col-lg-8 col-12">
        <form name="frm_add_product" id="frm_add_product" method="POST" action="index.php?page=product&method=store" enctype="multipart/form-data">

            <div class="form-group">
                <label>Name <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="name" id="name" required minlength="3" maxlength="200" placeholder="Name product">
            </div>

            <div class="form-group">
                <label>Category <span class="text-danger">*</span></label>
                <select class="form-control" name="category" id="category">
                    <?php foreach ($categories as $categorie) { ?>
                        <option value="<?= $categorie['id']; ?>">
                            <?= $categorie['title']; ?>
                        </option>
                    <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label>Price <span class="text-danger">*</span></label>
                <input type="number" step="0.5" class="form-control" name="price" id="price" required min="0">
            </div>

            <div class="form-group">
                <label>Avatar</label>
                <input type="file" required name="image" id="image">
            </div>

            <div class="form-group">
                <label>Quanity <span class="text-danger">*</span></label>
                <input type="number" class="form-control" name="quantity" id="quantity" required min="0">
            </div>

            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="description" id="description" rows="3"></textarea>
            </div>

            <div class="form-group">
                <label>Active</label>
                <label class="radio-inline">
                    <input type="radio" name="active" id="active_show" value="1" checked> Show
                </label>

                <label class="radio-inline">
                    <input type="radio" name="active" id="active_hide" value="0"> Hide
                </label>
            </div>

            <button type="submit" class="btn btn-primary" name="submit_add_product">Save</button>
            <button type="reset" class="btn btn-danger">Reset</button>

        </form>
    </div>
</div>
<!-- /.row -->