<?php
include_once('./Models/Product.php');

class ProductController extends Product
{
    public function __construct()
    {
        parent::__construct();
        $this->index();
    }

    public function index() {
        $method = 'list';
        if (isset($_GET['method'])) {
            $method = $_GET['method'];
        }

        switch ($method) {
            case 'list':
                $products = parent::all();
                include_once('./views/product/list_product.php');
                break;

            case 'add':
                $categories = $this->getAllCategory();
                include_once('./views/product/add_product.php');
                break;

            case 'store':
                $this->createProduct();
                break;

            case 'delete':
                if (isset($_GET['id'])) {
                    $id = trim((int)$_GET['id']);
                    $del = $this->delete($id);
                    if ($del) {
                        $_SESSION['notification'] = 2;
                        header("Location: index.php?page=product");
                    }
                }
                echo $_GET['id'];
                break;

            case 'edit':
                $this->editProduct();
                break;

            case 'update':
                $this->updateProduct();
                break;

            default :
                echo "<h2 style='color: red;'>404 NOT FOUND!</h2>";
                break;
        }
    }

    public function createProduct()
    {
        if (isset($_POST['submit_add_product'])) {
            $categoryId = $_POST['category'];
            $name = $_POST['name'];
            $price = $_POST['price'];
            $quantity = $_POST['quantity'];
            $description = $_POST['description'];
            $active = $_POST['active'];
            $files = $_FILES['image'];
            $nameFile = time().$files['name'];
            $tmpFile = $files['tmp_name'];
            move_uploaded_file($tmpFile, '../img/product/'.$nameFile);

            $add = $this->create($categoryId, $name, $price, $nameFile, $quantity, $description, $active);
            if ($add) {
                $_SESSION['notification'] = 1;
                header("Location: index.php?page=product");
            } else {
                header("Location: index.php?page=product&method=add");
            }

        } else {
            header("Location: index.php?page=product&method=add");
        }
    }

    public function editProduct() {
        if (isset($_GET['id'])) {
            $id = trim((int)$_GET['id']);
            $categories = $this->getAllCategory();
            $product = $this->find($id);
            include_once('./views/product/edit_product.php');
        } else {
            header("Location: index.php?page=product");
        }
    }

    /**
     * @return bool|void
     */
    public function updateProduct() {
        if (isset($_GET['id'])) {
            $id = trim((int)$_GET['id']);
            $product = $this->find($id);

            if (isset($_POST['submit_update_product'])) {
                $categoryId = $_POST['category'];
                $name = $_POST['name'];
                $price = $_POST['price'];
                $quantity = $_POST['quantity'];
                $description = $_POST['description'];
                $active = $_POST['active'];
                $nameFile = $oldFile = $product['images'];

                $files = $_FILES['image'];
                if (!empty($files['name'])) {
                    $nameFile = time().$files['name'];
                    $tmpFile = $files['tmp_name'];
                    move_uploaded_file($tmpFile, '../img/product/'.$nameFile);
                    unlink('../img/product/'.$oldFile);
                }

                $update = $this->update($id, $categoryId, $name, $price, $nameFile, $quantity, $description, $active);
                if ($update) {
                    $_SESSION['notification'] = 3;
                    header("Location: index.php?page=product");
                }
            }
        } else {
            header("Location: index.php?page=product");
        }
    }
}