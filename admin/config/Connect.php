<?php

class Connect
{
    const DNS = 'mysql:host=localhost;dbname=shop_fashion';
    const USER = 'root';
    const PASSWORD = '';
    public $pdo = null;

    /**
     * Connect constructor.
     */
    public function __construct()
    {
        try {
            $this->pdo = new PDO(self::DNS, self::USER, self::PASSWORD);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}