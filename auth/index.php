<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>HỆ THỐNG IT-PLUS</title>
		<link rel="stylesheet" href="./css/bootstrap.min.css" />
		<link rel="stylesheet" href="./css/my_css.css">
	</head>
	<body>

		<div class="container">
			
			<div class="row" style="margin-top: 150px; justify-content: center;">
				<div class="col-md-6 col-md-push-3">
					<?php 

						if (isset($_GET['page'])) {
							$page = $_GET['page'];
						}else{ $page = 'index'; }

						switch ($page) {
							case 'register':
								include 'views/register.php';
								break;
							
							default:
								include 'views/login.php';
								break;
						}
					 ?>
				</div>
			</div>

		</div>

		<script src="./js/jquery.js"></script>
		<script src="./js/myJava.js"></script>
		<script src="./js/bootstrap.min.js"></script>
	</body>
</html>