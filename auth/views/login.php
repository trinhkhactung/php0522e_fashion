<?php
    session_start();
    if (isset($_SESSION['id_account'])) {
        header('Location: ../admin');
    }

$accounts = [
    0 => [
        'name' => 'John Son',
        'email' => 'join@gmail.com',
        'password' => '123@123',
        'status' => 1
    ],

    1 => [
        'name' => 'Harry Kain',
        'email' => 'harry@gmail.com',
        'password' => '123@123',
        'status' => 1
    ],

    2 => [
        'name' => 'Mai Con',
        'email' => 'maicon@gmail.com',
        'password' => '123@123',
        'status' => 0
    ],
];

if (isset($_REQUEST['submit_login'])) {
//    echo "<pre>";
//    print_r($_REQUEST);
//    echo "</pre>";

    $email = !empty($_REQUEST['email']) ? trim($_REQUEST['email']) : '';
    $password = !empty($_REQUEST['password']) ? trim($_REQUEST['password']) : '';

    foreach ($accounts as $id => $account) {
        if ($email == $account['email'] && $password == $account['password'] && $account['status'] == 1) {
            $_SESSION['name_account'] = $account['name'];
            $_SESSION['id_account'] = $id;
            header('Location: ../admin');
        } else {
            $errors = 'Login fail!!!';
        }
    }
}
?>

<form action="" method="POST">
    <legend>Đăng nhập hệ thống</legend>

    <div class="form-group">
        <label for="email">Username</label>
        <input type="email" required id="email" name="email" class="form-control" value="<?= isset($email) ? $email : '' ?>"
               placeholder="Nhập email của bạn">
    </div>

    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" id="password" required name="password" class="form-control" value=""
               placeholder="Nhập pass">
    </div>

    <button type="submit" name="submit_login" class="btn btn-primary">Đăng nhập</button>
    <span>Nếu bạn chưa có tài khoản? <a href="index.php?page=register" style="color: red;">Đăng ký</a></span>

    <p style="color: red;"><?= isset($errors) ? $errors : '' ?></p>
</form>