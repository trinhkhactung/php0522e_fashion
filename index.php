<!DOCTYPE html>
<html lang="zxx">

<?php include_once ('./layout/head.php'); ?>

<body>
    <?php include_once ('./layout/header.php'); ?>

    <!-- START main content-->

    <?php
        $page = 'home';

        if (isset($_GET['page'])) {
            $page = $_GET['page'];

            if (!file_exists('./views/'.$page.'.php')) {
                include_once('./views/404.php');
            }
        }

        include_once('./views/'.$page.'.php');
    ?>
    <!-- END main content-->

    <?php
        include_once ('./layout/footer.php');
        include_once ('./layout/script.php');
    ?>
</body>

</html>