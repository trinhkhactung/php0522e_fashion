<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>AJAX</title>
</head>
<body>
    <form action="" method="POST" id="frm_process">
        Number 1: <input type="number" name="number_1" id="number_1"> <br><br>
        Number 2: <input type="number" name="number_2" id="number_2"> <br>
        <button type="submit" name="btn_add" id="btn_add">Add</button> <br><br><br><br>
    </form>

    <?php
        $account = [
            'name' => 'Tung',
            'age' => 30
        ];
        $json = json_encode($account);
        echo $json;
        $arr = json_decode($json, true);
        echo "<pre>";
        print_r($arr);
        echo "</pre>";

        if (isset($_POST['btn_add'])) {
            $number1 = $_POST['number_1'];
            $number2 = $_POST['number_2'];
            echo $number1 + $number2;
        }
    ?>
    <br><br>

    <iframe width="560" height="315" src="https://www.youtube.com/embed/HzVgG6qulSM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


    <button id="get_data">Click</button>
    <p id="data" style="color: green;"></p>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        $('#btn_add').click(function(e) {
            e.preventDefault(); // dung viec load trang

            var number1 = parseInt($('#number_1').val());
            var number2 = parseInt($('#number_2').val());
            $.ajax({
                url : 'process.php', // url request den
                type : 'POST', // phuong thuc gui
                dataType : 'html', // json, xml dang du lieu nhan ve
                data : { number1 : number1, number2 : number2 }, // data gui di

                success : function(data) {
                    console.log(data);
                    $('#data').html(data);
                },

                error : function() {
                    console.log("Error :)");
                }
            });
        });

        $('#get_data').click(function() {
            $.ajax({
                url : 'data.php', // url request den
                type : 'POST', // phuong thuc gui
                dataType : 'html', // json, xml dang du lieu nhan ve
                data : { id : 100, name : 'TungTK' }, // data gui di

                success : function(data) {
                    console.log(data);
                    $('#data').html(data);
                },

                error : function() {
                    console.log("Error :)");
                }
            });
        })
    </script>
</body>
</html>